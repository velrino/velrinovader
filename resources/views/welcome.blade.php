<!DOCTYPE html>
<html ng-app="Velrino">
   <head>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="<% url('assets/packages/materialize-css/dist/css/materialize.min.css') %>"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="<% url('assets/packages/font-awesome/css/font-awesome.min.css') %>"  media="screen,projection"/>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
      <link rel="stylesheet" href="<% url('assets/packages/font-mfizz-2.3.0/font-mfizz.css') %>">
      <link rel="stylesheet" href="<% url('assets/css/style.css') %>">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta charset="UTF-8" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="Desenvolvimento" />
	<meta property="og:site_name" content="Velrino" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Portfolio" />
	<meta property="og:url" content="http://velrino.com" />
	<meta property="og:description" content="Desenvolvimento Web" />
	<meta property="og:image" content="https://media.licdn.com/media/AAEAAQAAAAAAAAX4AAAAJDQ5NWE1YTgwLTkzMmItNDkwOS1hZGI1LWUwYzQ2NmVhNDAzYQ.png" />
	<meta property="fb:admins" content="100011030538907" />
   </head>
   <style>
   </style>
   <body class="grey darken-4 white-text">
      <div class="parallax-container " style="width:100%;" >
         <div class="section no-pad-bot">
            <div class="container">
               <br><br><br>       
               <h4 class="header center white-text">
                  <a href="#" class="typewrite white-text" data-period="2000" data-type='[ "Seja bem-vindo", "Jovem Padawan"]'>
                  <span class="wrap"></span>
                  </a>
               </h4>         

               <br><br><br>

               <br><br>
            </div>
         </div>
         <div class="parallax shazam hide-on-med-and-down"><img src="<% url('assets/images/velrino.jpg') %>"></div>
         <div class="parallax carai hide-on-large-only"><img src="<% url('assets/images/velrino.jpg') %>"></div>
      </div>

      <div class="section grey darken-4 white-text space_hr">
         <div class="row container">
            <h4 class="header center light-blue-text text-darken-2">
              I'M A JEDI
            </h4>
            <div class="center space_hr_min hr">
               <span class="outer-line"></span>
               <span class="fa fa-rebel fa-3x animated infinite pulse" aria-hidden="true"></span>
               <span class="outer-line"></span>
            </div>
               <div class="center">
                  <a href="https://www.github.com/velrino" target="_blank" class="space purple-text text-darken-2">
                  <i class="fa fa-github fa-3x animated infinite flashSlow"></i>
                  </a>
                  <a href="https://www.facebook.com/velrino" target="_blank" class="space">
                  <i class="fa fa-facebook fa-3x animated infinite flashSlow"></i>
                  </a>
                  <a href="https://www.instagram.com/velrino" target="_blank" class="space orange-text text-darken-2">
                  <i class="fa fa-instagram fa-3x animated infinite flashSlow"></i>
                  </a>
                  <a href="https://www.linkedin.com/in/velrino" target="_blank" class="space  light-blue-text text-darken-4">
                  <i class="fa fa-linkedin fa-3x animated infinite flashSlow"></i>
                  </a>
                  <a href="https://medium.com/@velrino" target="_blank" class="space green-text text-darken-2">
                  <i class="fa fa-medium fa-3x animated infinite flashSlow"></i>
                  </a>
                  <a href="https://www.youtube.com/user/Velrino" target="_blank" class="space red-text text-darken-2">
                  <i class="fa fa-youtube fa-3x animated infinite flashSlow"></i>
                  </a>
               </div>
               <br><br><br><br><br>
            <div class="col l3 right-align">
               <h6 class="grey-text">HOBBIES</h6>
               <h5> <strong>LEITURA, TRILHAS, VIAGENS, FILMES, SERIES, FOOTBALL, KART, AIRSOFT</strong></h5>
            </div>
            <div class="col l6 center-align">
               <div class="circlePhoto velrino center animated infinite"></div>
            </div>
            <div class="col l3 left-align">
               <h6 class="grey-text">FRASES</h6>
                <h5>              <a href="#" class="typewrite light-blue-text text-darken-2" data-period="2000" data-type='[ "WHO WANTS TO BE KING",  "SAY MY NAME", "WINTER IS COMING"]'></a></h5>
            </div>
            <div class="col l12 justify-align" style="padding-top:10%;">
               <blockquote>Sou apenas um padawan autodidata idealizando no mundo virtual.</blockquote>
               <blockquote>No final de 2014 eu criei e lancei meu primeiro game 3D que inclusive está de graça na <a href="https://play.google.com/store/apps/details?id=br.com.velrino.zodddemo" target="_blank">Play Store.</a></blockquote>
               <blockquote>Profissionalmente sou comunicativo, comprometido e objetivo; E espero o mesmo do cliente</blockquote>
               <blockquote>Meu tempo livre é investido em meus hobbys </blockquote>
               <blockquote>Quando o assunto é tecnologia minhas especialidades são engenharia back-end, costumo utilizar <a href="https://pt.wikipedia.org/wiki/Scrum_(desenvolvimento_de_software)" target="_blank" class="red-text text-darken-1"> Scrum </a> como metodologia ágil e ferramentas como <a href="https://trello.com/" target="_blank"  class="blue-text text-darken-1">Tello</a> para gestão dos planejamentos, <a target="_blank"  href="http://docker.com/" class="blue-text text-darken-1">Docker</a> para automatizar a construção de ambientes , <a target="_blank"  href="https://www.javascript.com/" class="yellow-text">Javascript</a> e <a target="_blank" href="https://secure.php.net/manual/pt_BR/intro-whatis.php"  class="light-blue-text">PHP</a> como método padronizado na comunicação de instruções, <a  target="_blank" href="https://www.mongodb.com/what-is-mongodb" class="green-text">MongoDB</a> no armazenamento <a href="https://pt.wikipedia.org/wiki/NoSQL" target="_blank"> No-SQL </a>, <text class="light-blue-text">MYSQL</text> para relacional e <a href="https://pt.wikipedia.org/wiki/Git"  target="_blank" class="purple-text">GIT</a> para versionamento </blockquote>
            </div>
         </div>
      </div>
      </div>
         <div class="section light-blue darken-2  white-text space_hr">
            <div class=" container">
             <h4 class="header center">PRÊMIOS & PARTICIPAÇÕES</h4>
            <div class="center space_hr_min hr inverse">
               <span class="outer-line"></span>
               <span class="fa fa-trophy fa-3x animated infinite pulse" aria-hidden="true"></span>
               <span class="outer-line"></span>
            </div>

    <div class="row">

      <div class="col l6 s12">
        <a href="https://github.com/velrino/BlueHack-1" target="_blank">
          <div class="card">
            <div class="card-image">
                <img class="responsive-img" src="https://instagram.fcgh5-1.fna.fbcdn.net/t51.2885-15/e35/17437433_962334553903937_6639067902496473088_n.jpg">
              <span class="card-title grey darken-4 truncate"> BLUEHACK: My Lawyer Bot, o Advogado Virtual, </span>
            </div>
          </div>
          </a>
      </div>

      <div class="col l6 s12">
        <a href="http://revistapegn.globo.com/Startups/noticia/2017/02/alugalogo-vence-primeira-competicao-de-startups-do-google-campus-sp.html" target="_blank">
          <div class="card">
            <div class="card-image">
                <img class="responsive-img" src="http://s2.glbimg.com/_Wqm5BezCO6TprdTZETKILBvVk4=/e.glbimg.com/og/ed/f/original/2017/02/23/alugalogo.jpg">
              <span class="card-title grey darken-4 truncate"> PEGN: ALUGALOGO VENCE PRIMEIRA COMPETIÇÃO DE STARTUPS DO GOOGLE CAMPUS SP </span>
            </div>
          </div>
          </a>
      </div>

    </div>

<!--http://s2.glbimg.com/_Wqm5BezCO6TprdTZETKILBvVk4=/e.glbimg.com/og/ed/f/original/2017/02/23/alugalogo.jpg-->
         </div>
      </div>
      </div>
      <div class="parallax-container" style="width:100%;" >

          <br><br><br><br><br><br>
              <h4 class="header center white-text">
                  <a href="#" class="typewrite white-text" data-period="2000" data-type='[ "RUMO", "A", "OESTE"]'>
                  <span class="wrap"></span>
                  </a>
               </h4>  
         <div class="parallax shazam hide-on-med-and-down"><img src="<% url('assets/images/roof.jpg') %>"></div>
         <div class="parallax carai hide-on-large-only"><img src="<% url('assets/images/roof.jpg') %>"></div>
      </div>
      <controller ng-controller="JobsController">
         <div class="section light-blue darken-2  white-text space_hr">
            <div class=" container">
             <h4 class="header center">EXPERIÊNCIAS</h4>
            <div class="center space_hr_min hr inverse">
               <span class="outer-line"></span>
               <span class="fa fa-star-half-o fa-3x animated infinite pulse" aria-hidden="true"></span>
               <span class="outer-line"></span>
            </div>

               <div class="row">
                  <div class="col l12 m12 s12 center">
                     <a class="btn-flat white-text darken-4"  ng-init="filters.category = ''" ng-class="{'grey':filters.category==''}" ng-click="filters.category = ''">Todos</a>
                     <a class="btn-flat white-text darken-4"  ng-class="{'grey':filters.category=='App'}" ng-click="filters.category = 'App'">App</a>
                     <a class="btn-flat white-text darken-4"  ng-class="{'grey':filters.category=='Web'}" ng-click="filters.category = 'Web'">Web</a>                     
                     <a class="btn-flat white-text darken-4"  ng-class="{'grey':filters.category=='Outros'}" ng-click="filters.category = 'Outros'">Outros</a>
                  </div>
               </div>

               <div class="row">
                  <div class="col s12 m12 l12" ng-class="{'animated flipInY': link.name}" ng-repeat="link in links | filter:filters">
                     <div class="card">
                        <div class="card-image waves-effect waves-block waves-light">
                           <img class="activator"  ng-src="{{link.img}}">
                           <span class="card-title grey darken-4">{{link.name}}</span>
                        </div>
                        <div class="card-reveal grey-text text-darken-4">
                           <span class="card-title ">{{link.name}}
                               <i class="fa fa-close fa-2x right"></i></span>
                           <p>{{link.text}}</p>
                           <a href="{{link.link}}" target="_blank"> Link </a>
                        </div>
                     </div>            

                  </div>
                  
               </div>
               
            </div>
         </div>
         </div>
      </controller>
      </div>
      </div>
      <div class="section space_hr">
         <div class="row container">
                <h4 class="header center light-blue-text text-darken-2">SKILLS</h4>
            <div class="center space_hr_min hr">
               <span class="outer-line"></span>
               <span class="fa fa-bolt fa-3x animated infinite pulse" aria-hidden="true"></span>
               <span class="outer-line"></span>
            </div>
            <ul class="circled center">
               <li class="purple"><a href="#"><i class="icon-apache fa-5x"></i></a></li>
               <li class="orange"><a href="#"><i class="icon-aws fa-5x"></i></a></li>
               <li class="green"><a href="#"><i class="icon-nodejs fa-5x"></i></a></li>
               <li class="red accent-4"><a href="#"><i class="icon-angular fa-5x"></i></a></li>
               <li class="orange accent-4"><a href="#"><i class="icon-laravel fa-5x"></i></a></li>
               <li class="yellow accent-4"><a href="#"><i class="icon-javascript fa-5x"></i></a></li>
               <li class="red accent-4"><a href="#"><i class="icon-redhat fa-5x"></i></a></li>
               <li class="green accent-4"><a href="#"><i class="icon-nginx-alt fa-5x"></i></a></li>
               <li class="blue accent-4"><a href="#"><i class="icon-html fa-5x"></i></a></li>
               <li class="blue accent-4"><a href="#"><i class="icon-phone-gap fa-5x"></i></a></li>
               <li class="blue accent-4"><a href="#"><i class="icon-php-alt fa-5x"></i></a></li>
               <li class="blue accent-4"><a href="#"><i class="icon-wordpress fa-5x"></i></a></li>
               <li class="blue accent-4"><a href="#"><i class="icon-docker fa-5x"></i></a></li>
               <li class="orange"><a href="#"><i class="icon-ubuntu fa-5x"></i></a></li>
               <li class="blue accent-4"><a href="#"><i class="icon-mysql fa-5x"></i></a></li>
               <li class="green accent-4"><a href="#"><i class="icon-mongodb fa-5x"></i></a></li>
            </ul>
         </div>
      </div>
      </div>
      <div ng-controller="Controller">
      </div>
      <!--Import jQuery before materialize.js-->
      <script src="<% url('assets/packages/angular/angular.min.js') %>"></script>
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="<% url('assets/packages/materialize-css/dist/js/materialize.min.js') %>"></script>
      <script type="text/javascript" src="<% url('assets/js/init.js') %>"></script>

   </body>
</html>