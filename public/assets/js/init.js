                  var TxtType = function(el, toRotate, period) {
                        this.toRotate = toRotate;
                        this.el = el;
                        this.loopNum = 0;
                        this.period = parseInt(period, 10) || 2000;
                        this.txt = '';
                        this.tick();
                        this.isDeleting = false;
                    };
                  
                    TxtType.prototype.tick = function() {
                        var i = this.loopNum % this.toRotate.length;
                        var fullTxt = this.toRotate[i];
                  
                        if (this.isDeleting) {
                        this.txt = fullTxt.substring(0, this.txt.length - 1);
                        } else {
                        this.txt = fullTxt.substring(0, this.txt.length + 1);
                        }
                  
                        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';
                  
                        var that = this;
                        var delta = 200 - Math.random() * 100;
                  
                        if (this.isDeleting) { delta /= 2; }
                  
                        if (!this.isDeleting && this.txt === fullTxt) {
                        delta = this.period;
                        this.isDeleting = true;
                        } else if (this.isDeleting && this.txt === '') {
                        this.isDeleting = false;
                        this.loopNum++;
                        delta = 500;
                        }
                  
                        setTimeout(function() {
                        that.tick();
                        }, delta);
                    };
                  
                    window.onload = function() {
                        var elements = document.getElementsByClassName('typewrite');
                        for (var i=0; i<elements.length; i++) {
                            var toRotate = elements[i].getAttribute('data-type');
                            var period = elements[i].getAttribute('data-period');
                            if (toRotate) {
                              new TxtType(elements[i], JSON.parse(toRotate), period);
                            }
                        }
                        // INJECT CSS
                        var css = document.createElement("style");
                        css.type = "text/css";
                        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
                        document.body.appendChild(css);
                    };
         var app = angular.module('Velrino', []);
         app.controller('Controller', ['$scope', '$http', Controller]);    
         
         function Controller($scope, $http, $sce) 
         {
             $.fn.extend({
         animateCss: function (animationName) {
         var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
         this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
         });
         }
         });
             $('.pulseSlow').animateCss('pulse');
             $('.parallax').parallax();
         }
         
         app.controller('JobsController', function($scope) {
         
            $scope.filters = { };
         
            $scope.links = [
                {
                    name: 'Flitt (Alugalogo)', 
                    img: '/assets/images/flitt.png', 
                    category: 'Web',
                    text: 'Flitt é um produto oferecido pela startup Alugalogo onde o gerente de obras tem a centralização da comunicação e monitoração da obra e os seus operadores ',
                    link: 'http://app.flitt.com.br/'
                },
                {
                    name: 'Descripto', 
                    img: '/assets/images/descripto.png', 
                    category: 'App',
                    text: 'O Descripto é um game free desenvolvido na Unity e com modelado no blender que está disponível de graça na Play Store',
                    link: 'https://play.google.com/store/apps/details?id=br.com.velrino.zodddemo'
                },
                {
                    name: 'MAPN', 
                    img: '/assets/images/mapn.png', 
                    category: 'WEB',
                    text: 'MAPN checkin de locais com compartilhamento fotográfico',
                    link: 'http://mapn.com.br/'
                },
                {
                    name: 'API Simple', 
                    img: '/assets/images/apisimple.png', 
                    category: 'Outros',
                    text: 'Uma API simples desenvolvida em Lumen e MongoDB',
                    link: 'https://github.com/velrino/simple-api'
                },
                {
                    name: 'Produtos dos Sonhos', 
                    img: '/assets/images/produtodossonhos.png', 
                    category: 'Web',
                    text: 'Produtos dos Sonhos é um ecommerce de produtos capilares',
                    link: 'http://produtodossonhos.com.br/'
                },

            ];
         });